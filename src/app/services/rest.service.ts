import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private httpCliente: HttpClient) {
  }

  sendPostRequest(url: string, body: any, headers?: Map<string, string>) {
    return this.httpCliente.post<any>(url, body);
  }

  sendGetRequest(url: string, params: Map<string, string>, headers?: Map<string, string>) {
    let param = new HttpParams();

    if(params != null){
      params.forEach((value, key) =>
      param = param.set(key, value));
    }

    return this.httpCliente.get<any>(url, { params: param });
  }

}
