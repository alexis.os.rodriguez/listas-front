import { Component, OnInit } from '@angular/core';
import { RestService } from '../services/rest.service';
import { Persona } from '../model/persona';
import { stringify } from 'querystring';
import { Direccion } from '../model/direccion';

@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.scss']
})
export class PersonaComponent implements OnInit {

  personaModel: Persona = new Persona();
  listaPersona: Array<Persona> = [];
  listaCargada:boolean= false;
  

  constructor(private rest: RestService) { }

  ngOnInit() {
    this.traerLista();
  }

  async traerLista() {
    this.rest.sendGetRequest("http://localhost:8080/listPersonaA", null).subscribe(rest => {

      let lista:Array<Persona> = rest;
      this.listaPersona = this.cargarLista(lista);

    });

  }

  traerDireccion(url: string, id: number) {
    let params: Map<string, string> = new Map<string, string>();

    params.set("personaId", id.toString());

    return this.rest.sendGetRequest(url, params);
  }

  cargarLista(listaPersona: Array<Persona>): Array<Persona> {

    listaPersona.forEach(p => {
      this.traerDireccion("http://localhost:8080/listaDireccionC", p.id).subscribe(direccion => {
        if (direccion != null) {
          this.setearValor(p, direccion);
        }
        else {
          this.traerDireccion("http://localhost:8080/listaDireccionD", p.id).subscribe(direccion => {
            if (direccion != null) {
              this.setearValor(p, direccion);
            } else {
              this.setearValorDefault(p);
            }
          })
        }
      });
    });
    return listaPersona;
  }

  setearValor(persona: Persona, direccion: Direccion) {
    let dir: Direccion = new Direccion();

    dir.calleAltura = direccion.calle + " " + direccion.altura;
    dir.ciudad = direccion.ciudad;
    dir.provincia = direccion.provincia;
    dir.pais = direccion.pais;
    dir.codigoPostal = direccion.codigoPostal;

    persona.direccion = dir;
  }

  setearValorDefault(persona: Persona): Persona {

    persona.direccion.calle = "Calle Falsa";
    persona.direccion.altura = "123";
    persona.direccion.ciudad = "Azul";
    persona.direccion.provincia = "Buenos Aires";
    persona.direccion.pais = "Argentina";

    return persona;
  }

  obtenerListadoPersonas(campoOrden:string){
    switch(campoOrden){
      case "nombre":
            this.ordenarPorNombre(this.listaPersona);
            break;
      case "apellido":
            this.ordenarPorApellido(this.listaPersona);
            break;
      case "id":
            this.ordenarPorId(this.listaPersona);
            break;
    }

    console.log("$$$",this.listaPersona);
  }

  ordenarPorNombre(listaPersonas:Array<Persona>){

    listaPersonas.sort((a,b)=>{
      if(a.nombre > b.nombre){
        return 1;       
      }else if(a.nombre < b.nombre){
        return -1;
      }
      //si es igual a y b retorna 0
      return 0
    });
     
  }

  ordenarPorApellido(listaPersonas:Array<Persona>){

    listaPersonas.sort((a,b)=>{
      if(a.apellido > b.apellido){
        return 1;       
      }else if(a.apellido < b.apellido){
        return -1;
      }
      //si es igual a y b retorna 0
      return 0
    });
    
  }

  ordenarPorId(listaPersonas:Array<Persona>){

    listaPersonas.sort((a,b)=>{
      if(a.id > b.id){
        return 1;       
      }else if(a.id < b.id){
        return -1;
      }
      //si es igual a y b retorna 0
      return 0
    });
    
  }




  /*No se puede identificar el codigoPostal de manera unica, ya que el servicio que nos devuelve del codigo postal,
   le faltaria la Altura para identificar el CP, un CP se identifica con la Calle y la Altura (Aprox)*/

}
